package com.kh.tsp.common;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AfterAdvice {
	@Pointcut("execution(* com.kh.tsp..*ServiceImpl.*(..))")
	public void allPointcut() {}
	
	@After("allPointcut()")
	public void finallyLog() {
		//예외 발생 여부에 상관 없이 무조건 수행하는 기능을 작성하는 어드바이스
		
		System.out.println("[후 실행] 비지니스 로직 수행 후 무조건 작동");
	}
	
}










