package com.kh.tsp.member.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.kh.tsp.common.CommonUtils;
import com.kh.tsp.member.model.exception.LoginFailedException;
import com.kh.tsp.member.model.service.MemberService;
import com.kh.tsp.member.model.vo.Member;

@SessionAttributes("loginUser")
@Controller
public class MemberController {
	@Autowired
	private MemberService ms;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	//1. HttpServletRequest, HttpServletResponse를 매개변수로 선언하는 방식
//	@RequestMapping("/login.me")
//	public String loginCheck(HttpServletRequest request, HttpServletResponse response) {
//		String userId = request.getParameter("userId");
//		String userPwd = request.getParameter("userPwd");
//		
//		System.out.println("userId : " + userId);
//		System.out.println("userPwd : " + userPwd);
//		
//		return "main/main";
//	}
	
	//2. RequestParam 어노테이션을 이용해서 파라미터 값 받기
//	@RequestMapping(value="/login.me", method=RequestMethod.POST)
//	public String loginCheck(@RequestParam("userId") String userId, 
//							@RequestParam String userPwd) {
//		
//		System.out.println("userId : " + userId);
//		System.out.println("userPwd : " + userPwd);
//		
//		
//		return "main/main";
//	}
	
	//3. RequestParam 어노테이션을 생략하고 사용하는 방법
//	@RequestMapping(value="/login.me", method=RequestMethod.POST)
//	public String loginCheck(String userId, String userPwd) {
//		
//		System.out.println("userId : " + userId);
//		System.out.println("userPwd : " + userPwd);
//		
//		return "main/main";
//	}
	
	
	//4. @ModelAttribute를 이용한 값 전달 받는 방법
//	@PostMapping("/login.me")
//	public String loginCheck(@ModelAttribute Member m) {
//		
//		System.out.println("member : " + m);
//		
//		return "main/main";
//	}
	
	//5. @ModelAttribute를 생략하는 방법
//	@PostMapping("/login.me")
//	public String loginCheck(Member m, HttpServletRequest request) {
//		
//		System.out.println("member : " + m);
//		
//		//System.out.println("ms.hashcode() : " + ms.hashCode());
//		
////		MemberService ms = new MemberServiceImpl();
//		try {
//			Member loginUser = ms.loginMember(m);
//			
//			request.getSession().setAttribute("loginUser", loginUser);
//			
//			return "main/main";
//			
//		} catch (LoginFailedException e) {
//			request.setAttribute("msg", e.getMessage());
//			
//			return "common/errorPage";
//		}
//		
////		return "main/main";
//	}
	
//	@RequestMapping("/logout.me")
//	public String logout(HttpServletRequest request) {
//		request.getSession().invalidate();
//		
//		return "main/main";
//	}
	
	//6. ModelAndView로 리턴 처리
	//=> Model은 뷰로 전달할 데이터를 맵 형식으로 담을 때 사용하는 객체로 scope는 기본 request이다.
	//=> 서블릿에서 사용하던 requestScope라고 생각하면 된다.
	//=> View 는 RequestDispatcher처럼 forward 혹은 redirect 할 경로를 담는 객체이다.
	//=> ModelAndView는 이 두 가지를 합쳐놓은 객체이다.
	//=> Model은 따로 사용하는 것 도 가능하다.
	//=> 하지만 View는 따로 사용하지 못한다.
//	@PostMapping("/login.me")
//	public ModelAndView loginCheck(Member m, ModelAndView mv, HttpSession session) {
//		
//		try {
//			Member loginUser = ms.loginMember(m);
//			
//			session.setAttribute("loginUser", loginUser);
//			
//			//mv.setViewName("main/main");
//			mv.setViewName("redirect:index.jsp");
//		
//		} catch (LoginFailedException e) {
//			mv.addObject("msg", e.getMessage());
//			mv.setViewName("common/errorPage");
//		}
//		
//		return mv;
//	}
	
	//7. Model 객체를 따로 사용하고 String으로 뷰 이름을 리턴하는 방법
//	@PostMapping("/login.me")
//	public String loginCheck(Member m, Model model, HttpSession session) {
//		try {
//			Member loginUser = ms.loginMember(m);
//			
//			session.setAttribute("loginUser", loginUser);
//			
//			return "redirect:index.jsp";
//		} catch (LoginFailedException e) {
//			model.addAttribute("msg", e.getMessage());
//			
//			return "common/errorPage";
//		}
//	}
	
	//8. session에 저장할 때 @SessionAttribute 사용하기
	//=> Model에 Attribute를 추가할 때 자동으로 설정한 키 값을 Model에서 찾아 Session에 등록하는 기능을 제공하는 어노테이션
//	@PostMapping("/login.me")
//	public String loginCheck(Member m, Model model) {
//		
//		try {
//			Member loginUser = ms.loginMember(m);
//			
//			model.addAttribute("loginUser", loginUser);
//			
//			return "redirect:index.jsp";
//		} catch (LoginFailedException e) {
//			model.addAttribute("msg", e.getMessage());
//			
//			return "common/errorPage";
//		}
//	}
	
	@RequestMapping("logout.me")
	public String logout(SessionStatus status) {
		
		status.setComplete();
		
		return "redirect:index.jsp";
	}
	
	@RequestMapping("memberJoinForm.me")
	public String showMemberJoinForm() {
		
		return "member/memberJoinForm";
	}
	
//	@RequestMapping("insert.me")
//	public String insertMember(Model model, Member m) {
//		
//		System.out.println("insert member : " + m);
//		
//		//bcrypt란?
//		//DB에 비밀번호를 저장할 목적으로 설계되었다.
//		
//		//단방향 암호화 방식
//		//특정한 수학적인 연산을 통해 암호화된 메세지인 다이제스트를 생성한다.
//		//원본 메세지를 다이제스트로 변경하는걸 암호화 한다고 한다.
//		//다이제스트로 변경된 암호화된 문자열을 다시 원본 문자열로 되돌리는 것을 복호화 한다고 한다.
//		//복호화가 불가능 한 방식을 단방향, 복호화가 가능한 방식을 양방향 이라고 한다.
//		
//		//sha-512도 단방향 해시 암호화 알고리즘이다.
//		
//		//sha-512를 비밀번호 암호화에 사용하면 안되는 이유
//		//sha-512는 검색을 위해 설계된 암호화 알고리즘이다.
//		//또한 특정한 규칙을 가지고 암호화 되도록 설계되었다.
//		
//		//1. 단방향 해시 함수는 많은 다이제스트가 확보되면 평문을 찾아낼 수 있다.
//		//2. 다이제스트를 생성하는데 걸리는 시간이 굉장히 짧다
//		
//		//이를 해결하기 위해서 bcrypt는 다이제스트 생성 시간을 지연시키고
//		//랜덤 솔팅(salting) 기법을 활용한다.
//		
//		//bcrypt는 스프링 시큐리티라는 모듈에서 제공하고 있다.
//		
////		System.out.println("원본 비밀번호 : " + m.getUserPwd());
////		System.out.println("암호화된 비밀번호 : " 
////					+ passwordEncoder.encode(m.getUserPwd()));
//		
//		//비밀번호 암호화
//		m.setUserPwd(passwordEncoder.encode(m.getUserPwd()));
//		
//		if(m.getGender().equals("1") || m.getGender().equals("3")) {
//			m.setGender("M");
//		} else {
//			m.setGender("F");
//		}
//		
//		int result = ms.insertMember(m);
//		
//		if(result > 0) {
//			
//			return "main/main";
//		} else {
//			model.addAttribute("msg", "회원 가입 실패!");
//			
//			return "common/errorPage";
//		}
//		
//		
//	}
	
	@RequestMapping("login.me")
	public String loginCheck(Model model, Member m) {
		
		//m.setUserPwd(passwordEncoder.encode(m.getUserPwd()));
		
		System.out.println("login check controller : " + m);
		
		try {
			Member loginUser = ms.loginMember(m);
			
			model.addAttribute("loginUser", loginUser);
			
			return "redirect:index.jsp";
		} catch (LoginFailedException e) {
			model.addAttribute("msg", e.getMessage());
			
			return "common/errorPage";
		}
		
		
		
	}
	
	@RequestMapping(value="insert.me")
	public String insertMember(Model model, Member m, 
					HttpServletRequest request, MultipartFile photo) {
		
		System.out.println(m);
		System.out.println(photo);
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		System.out.println(root);
		
		String filePath = root + "\\uploadFiles";
		
		String originFileName = photo.getOriginalFilename();
		String ext = originFileName.substring(originFileName.lastIndexOf("."));
		String changeName = CommonUtils.getRandomString();
		
		try {
			photo.transferTo(new File(filePath + "\\" + changeName + ext));
			
			m.setUserPwd(passwordEncoder.encode(m.getUserPwd()));
			
			if(m.getGender().equals("1") || m.getGender().equals("3")) {
				m.setGender("M");
			} else {
				m.setGender("F");
			}
			
			ms.insertMember(m);
			
		} catch (Exception e) {
			new File(filePath + "\\" + changeName + ext).delete();
			
			model.addAttribute("msg", "회원 가입 실패");
			
			return "common/errorPage";
		}
		
		return "redirect:index.jsp";
	}
	
	//1. 스트림을 이용한 ajax 응답
//	@RequestMapping(value="duplicationCheck.me")
//	public void duplicationCheck(@RequestParam String userId, HttpServletResponse response) {
//		System.out.println(userId);
//		
//		try {
//			response.getWriter().print(false);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
	//2. ObjectMapper를 이용한 ajax 처리(jackson lib 이용)
//	@RequestMapping("duplicationCheck.me")
//	public void duplicationCheck(@RequestParam String userId,
//									HttpServletResponse response) {
//		
//		Member m = new Member();
//		m.setUserId(userId);
//		
//		ObjectMapper mapper = new ObjectMapper();
//		
//		try {
//			response.getWriter().print(mapper.writeValueAsString(m));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
	//3. jsonView를 사용한 방식
	@RequestMapping("duplicationCheck.me")
	public ModelAndView duplicationCheck(String userId, ModelAndView mv) {
		
		Member m = new Member();
		m.setUserId(userId);
		
		mv.addObject("member", m);
		mv.setViewName("jsonView");
		
		return mv;
	}

	// 로그인 폼 페이지로 이동
	@RequestMapping("loginView.me")
	public String test() {
		return "member/login";
	}






}

















