package com.kh.tsp.member.model.dao;

import org.mybatis.spring.SqlSessionTemplate;

import com.kh.tsp.member.model.exception.LoginFailedException;
import com.kh.tsp.member.model.vo.Member;

public interface MemberDao {

	Member loginCheck(SqlSessionTemplate sqlSession, Member m) throws LoginFailedException;

	int insertMember(SqlSessionTemplate sqlSession, Member m);

	String selectEncPassword(SqlSessionTemplate sqlSession, Member m);

	Member selectMember(SqlSessionTemplate sqlSession, Member m);

	int insertBoard(SqlSessionTemplate sqlSession);

}
