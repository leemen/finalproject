package com.kh.tsp.member.model.service;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kh.tsp.member.model.dao.MemberDao;
import com.kh.tsp.member.model.exception.LoginFailedException;
import com.kh.tsp.member.model.vo.Member;

@Service
public class MemberServiceImpl implements MemberService {
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private MemberDao md;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
//	@Override
//	public Member loginMember(Member m) throws LoginFailedException {
//		
//		return md.loginCheck(sqlSession, m);
//	}

//	@Override
//	public int insertMember(Member m) {
//		
//		return md.insertMember(sqlSession, m);
//	}

	@Override
	public Member loginMember(Member m) throws LoginFailedException {
		
		System.out.println("loginMember가 호출됨...");
		
		Member loginUser = null;
		
		String encPassword = md.selectEncPassword(sqlSession, m);
		
		if(!passwordEncoder.matches(m.getUserPwd(), encPassword)) {
			throw new LoginFailedException("로그인 실패!");
		} else {
			loginUser = md.selectMember(sqlSession, m);
		}
		
		return loginUser;
	}
	
	//1. 트랜젝션 설정 이전
//	@Override
//	public int insertMember(Member m) {
//		
//		int result = md.insertMember(sqlSession, m);
//		int result2 = md.insertBoard(sqlSession);
//		
//		return result;
//	}
	
	//2. 트랜젝션 기본값 설정 후 트랜젝션 상태를 관리하는 객체를 통해 트랜젝션 처리 하는 방법
//	@Override
//	public int insertMember(Member m) {
//		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
//		
//		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
//		
//		TransactionStatus status = transactionManager.getTransaction(def);
//		
//		try {
//			sqlSession.getConnection().setAutoCommit(false);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
//		int result1 = md.insertMember(sqlSession, m);
//		int result2 = md.insertBoard(sqlSession);
//		
//		int result = 0;
//		if(result1 > 0 && result2 > 0) {
//			transactionManager.commit(status);
//			result = 1;
//		} else {
//			transactionManager.rollback(status);
//		}
//		
//		return result;
//	}
	
	//2. @Transactional 어노테이션을 이용하는 방법
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED, 
//				isolation = Isolation.SERIALIZABLE,
//				rollbackFor= {Exception.class})
//	public int insertMember(Member m) {
//		
//		int result1 = md.insertMember(sqlSession, m);
//		int result2 = md.insertBoard(sqlSession);
//		
//		int result = 0;
//		if(result1 > 0 && result2 > 0) {
//			result = 1;
//		} else {
//			result = 0;
//		}
//		
//		return result;
//	}
	
	@Override
	public int insertMember(Member m) {
		
//		int result1 = md.insertMember(sqlSession, m);
//		int result2 = md.insertBoard(sqlSession);
//		
//		int result = 0;
//		if(result1 > 0 && result2 > 0) {
//			result = 1;
//		}
//		
//		return result;
		
		return md.insertMember(sqlSession, m);
		
	}
}























