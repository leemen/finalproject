package com.kh.tsp.member.model.exception;

public class LoginFailedException extends Exception {
	public LoginFailedException(String msg) {
		super(msg);
	}
}
