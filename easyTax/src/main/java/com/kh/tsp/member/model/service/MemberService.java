package com.kh.tsp.member.model.service;

import com.kh.tsp.member.model.exception.LoginFailedException;
import com.kh.tsp.member.model.vo.Member;

public interface MemberService {

	Member loginMember(Member m) throws LoginFailedException;

	int insertMember(Member m);

}
