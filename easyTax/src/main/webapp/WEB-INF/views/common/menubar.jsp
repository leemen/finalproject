<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
	 .main_wrapper{
    
height: 700px;
background: #FFFFFF;
}

.wrapper{

    /* position: absolute; */
    /* left: 27.01%;
    right: 27.08%;
    top: 12.78%;
    bottom: 9.89%; */
    
    width: 900px;
    height: 500px;
    
    background: #FFFFFF;
    border: 1px solid #000000;
    box-sizing: border-box;
    border-radius: 30px;
    margin: auto;
    margin-top: auto;
    padding: 0;
    
}

.img1_wrapper{
    text-align: center;
    margin-top: 85x;
    cursor: pointer;
}

.text1_wrapper{
    text-align: center;
   font-weight: bold;
   font-size: 20px;
   font-family: NEXON Lv1 Gothic Low OTF;
}

.loginbox{
    width:330px;
    height:200px;
    border:1px solid #ccc;
    margin-left:auto;
    margin-right:auto;
    padding:25px 15px;
    box-sizing: border-box;
    font-family: NEXON Lv1 Gothic Low OTF;
    text-align: center;
}

.loginbox h1{
    width:330px;
    height:40px;
    font-size:28px;
    color:#ff9900;
    border-bottom:1px solid #ff9900;
    padding-left:40px;
    box-sizing:border-box;
    background-image:url(images/login_icon.png);
    background-repeat:no-repeat;
    background-position: 3px 5px;
    margin-bottom:30px;
    font-family: NEXON Lv1 Gothic Low OTF;

}

#img1{
   width: 50%;
   height: 50%;
   margin: auto;
}

   

</style>
<title>Insert title here</title>
</head>
<body>
	<c:set var="contextPath" value="${ pageContext.servletContext.contextPath }" scope="application"/>
	
	<br>
	
	<div class="main_wrapper">
    <div class="wrapper">
        <div class="img1_wrapper">
             <img src="${contextPath}/resources/images/logo.png" onclick="">
        </div>
        <div class="text1_wrapper">
            <p>로그인</p>
        </div>
        
        <div class="loginbox">
            <form action="#">
            <c:if test="${!empty loginUser}">
            	<c:out value="${sessionScope.loginUser.userName }님 환영합니다."/>
            	<button type="button" onclick="location.href='logout.me'">로그아웃</button>
            </c:if>
            <c:if test="${empty loginUser}">
            	<a href="loginView.me">로그인</a>
            	<a href="#">ID/PW찾기</a><br>
                <a href="memberJoinForm.me">회원가입</a>
            </c:if>

            </form>
        </div>
        
    </div>
    </div>
</body>
</html>