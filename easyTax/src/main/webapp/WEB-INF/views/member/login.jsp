<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
	.main_wrapper{
    
height: 900px;

background: #FFFFFF;
}

.wrapper{

    position: absolute;
    left: 27.01%;
    right: 27.08%;
    top: 12.78%;
    bottom: 9.89%;
    
    background: #FFFFFF;
    border: 1px solid #000000;
    box-sizing: border-box;
    border-radius: 30px;
}

.img1_wrapper{
    text-align: center;
    margin-top: 85x;
    cursor: pointer;
}

.text1_wrapper{
    text-align: center;
   font-weight: bold;
   font-size: 20px;
   font-family: NEXON Lv1 Gothic Low OTF;
}

.loginbox{
    width:330px;
    height:320px;
    border:1px solid #ccc;
    margin-left:auto;
    margin-right:auto;
    padding:25px 15px;
    box-sizing: border-box;
    font-family: NEXON Lv1 Gothic Low OTF;
    text-align: center;
}

.loginbox h1{
    width:100%;
    height:40px;
    font-size:28px;
    color:#ff9900;
    border-bottom:1px solid #ff9900;
    padding-left:40px;
    box-sizing:border-box;
    background-image:url(images/login_icon.png);
    background-repeat:no-repeat;
    background-position: 3px 5px;
    margin-bottom:30px;
    font-family: NEXON Lv1 Gothic Low OTF;

}


	
</style>
<title>Insert title here</title>
</head>
<body>
	<c:set var="contextPath" value="${ pageContext.servletContext.contextPath }" scope="application"/>
	
	
	<br>
	
	<div class="main_wrapper">
    <div class="wrapper">
        <div class="img1_wrapper">
             <img src="${contextPath}/resources/images/logo.png" onclick="">
        </div>
        <div class="text1_wrapper">
            <p>로그인 폼 접속</p>
        </div>
        
        <div class="loginbox">
            <form action="login.me">
                <label for="loginid" class="labelid"></label>
                <input type="text" id="loginid" name="userId" placeholder="아이디를 입력하세요"><br/>
                <label for="loginpw" class="labelpw"></label><br>
                <input type="password" id="loginpw" name="userPwd" placeholder="비밀번호를 입력하세요"><br>
                <button type="submit" id="loginbtn">로그인</button>
                <div class="serach_signup">
                    <a href="#">ID/PW찾기</a><br>
                    <a href="memberJoinForm.me">회원가입</a>
                </div>
            </form>
        </div>
        
    </div>
    </div>
</body>
</html>